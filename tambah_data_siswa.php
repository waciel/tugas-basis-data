<!DOCTYPE html>
<html>
<head>
	<title>Tambah Data Siswa - SMKN 01 CIBINONG</title>
	<link rel="stylesheet" type="text/css" href="./bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="./bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="./bootstrap/css/themify-icons.css">
	<link rel="stylesheet" type="text/css" href="./bootstrap/css/font-awesome.min.css">
	<script src="./bootstrap/js/popper.min.js"></script>
	<script src="./bootstrap/js/bootstrap.min.js"></script>
	<script src="./bootstrap/js/bootstrap.js"></script>
	<script src="./bootstrap/js/jquery-3.3.1.slim.min.js"></script>
	<!-- cdn anjay -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<!-- //cdn -->
</head>
<body>
<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-dark bg-primary shadow fixed-top" style="background: linear-gradient(141deg, #9fb8ad 0%, #1fc8db 51%, #2cb5e8 10%);">
  <a class="navbar-brand" href="#"><img src="../image/logoteks.png" width="200px" height="80px"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse ml-5" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="home.php" style="font-size: 19px;">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="#" style="font-size: 19px;">Data Siswa</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#" style="font-size: 19px;">Data Guru</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#" style="font-size: 19px;">Data Nilai</a>
      </li>
    </ul>
    <ul class="navbar-nav flex-row ml-md-auto d-none d-md-flex">
	<div class="dropdown">
	  <button class="btn btn-transparent dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border:none; color: #fff; font-size: 18px;">
	    Hello, Wahyu Aditya Rizki!!
	  </button>
	  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
	    <a class="dropdown-item" href="#">Logout</a>
	    <a class="dropdown-item" href="#">Another action</a>
	    <a class="dropdown-item" href="#">Something else here</a>
	  </div>
	</div>
    </ul>
  </div>
</nav>
<!-- //Nabar END -->
<div class="container" style="margin-top: 150px;">
	<div class="row-md-12">
		<a href="data_siswa.php" class="btn btn-lg btn-outline-dark">Back</a>
			<div class="col md-12"><h1>Tambah Data Siswa</h1></div>
		</div>
	<div class="row-md-12" style="padding:20px;">
		<div class="col md-12">
		<form action="" method="POST">
		  <div class="form-group">
		    <label for="exampleInputEmail1">NIS</label>
		    <input type="number" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="NIS">
		  </div>
		  <div class="form-group">
		    <label for="exampleInputPassword1">Nama Lengkap</label>
		    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Password">
		  </div>
		  <div class="form-group">
		    <label for="exampleInputPassword1">Jenis Kelamin</label>
		    <select class="form-control">
		    	<option value="Laki - Laki">Laki - Laki</option>
		    	<option value="Perempuan">Perempuan</option>
		    </select>
		  </div>
		  <div class="form-group">
		    <label for="exampleInputPassword1">Kelas</label>
		    <select class="form-control">
		    	<option value="1">12 TKR 1</option>
		    	<option value="2">12 RPL 2</option>
		    	<option value="3">11 MM 2</option>
		    </select>
		  </div>
		  <button type="submit" class="btn btn-primary">Insert</button>
		  <button type="submit" class="btn btn-danger">Reset</button>
		</form>
		</div>
		<div class="col md-12"></div>
	</div>
</div>
</body>
</html>