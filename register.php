<!DOCTYPE html>
<html>
<head>
	<title>Register - SMKN 01 CIBINONG</title>
	<link rel="stylesheet" type="text/css" href="./bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="./bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="./bootstrap/css/themify-icons.css">
	<link rel="stylesheet" type="text/css" href="./bootstrap/css/font-awesome.min.css">
	<script src="./bootstrap/js/popper.min.js"></script>
	<script src="./bootstrap/js/bootstrap.min.js"></script>
	<script src="./bootstrap/js/bootstrap.js"></script>
	<script src="./bootstrap/js/jquery-3.3.1.slim.min.js"></script>
  <style type="text/css">
    body{
      background-image: url(./image/smkdepan.jpg);
      background-size: cover;
    }
  </style>
</head>
<body>
  <div class="container" style="margin-top: 100px;">
    <div class="row md-12">
      <div class="col md-4"></div>
      <div class="col md-4 p-4" style="border:1px solid #c0c0c0; border-radius: 5px; background-color: #c0c0c0; opacity: 0.8;">
<form action="" method="POST">
  <h1 class="text-center" style="margin-bottom:100px;">Register</h1>
  <div class="form-group">
    <input type="number" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="NIS" name="nis" required="required">
  </div>
  <div class="form-group">
    <input type="nama" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama Lengkap" name="nama" required="required">
  </div>
  <div class="form-group">
    <select name="jk" class="form-control" required="required">
      <option value="Laki-Laki">Laki-Laki</option>
      <option value="Perempuan">Perempuan</option>
    </select>
  </div>
  <div class="form-group">
    <select name="kelas" class="form-control" required="required">
      <?php
      include 'koneksi.php';
        $query=mysqli_query($conn,"SELECT * FROM kelas");
        $count=mysqli_num_rows($query);
        if ($count > 0) {
          while ($data=mysqli_fetch_array($query)) {
      ?>
      <option value="<?php echo $data['id']?>"><?php echo $data['tingkat']." ".$data['idprodi']." ".$data['nomor']?></option>
  <?php
    }}
  ?>
    </select>
  </div>
  <div class="form-group">
    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password" required="required">
  </div>
  <input type="submit" class="btn btn-primary" style="width: 300px; height: 40px; font-size: 18px; margin-left: 20px;" name="submit" value="Submit">
    <div class="row md-12">
      <div class="col md-12 text-center mt-5">
        <a href="login.php">Sudah Punya Akun? Login</a>
      </div>
    </div>
</form>
      </div>
      <div class="col md-4"></div>
    </div>
  </div>
</body>
<?php
  if (isset($_POST['submit'])) {
    $nis=$_POST['nis'];
    $nama=$_POST['nama'];
    $jk=$_POST['jk'];
    $kelas=$_POST['kelas'];
    $password=$_POST['password'];
    $insert=mysqli_query($conn,"INSERT INTO siswa(idkelas,nama,nis,password,jeniskelamin) VALUES('$kelas','$nama','$nis','$password','$jk')");
    if ($insert) {
      ?>
      <script type="text/javascript">
        alert('Anda Berhasil Terdaftar!');
        location='login.php';
      </script>
      <?php
    }else{
      ?>
      <script type="text/javascript">
        alert("Anda Gagal Terdaftar, masukkan kembali data diri anda!");
      </script>
      <?php
    }
  }
?>
</html>