<!DOCTYPE html>
<html>
<head>
	<title>Register - SMKN 01 CIBINONG</title>
	<link rel="stylesheet" type="text/css" href="./bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="./bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="./bootstrap/css/themify-icons.css">
	<link rel="stylesheet" type="text/css" href="./bootstrap/css/font-awesome.min.css">
	<script src="./bootstrap/js/popper.min.js"></script>
	<script src="./bootstrap/js/bootstrap.min.js"></script>
	<script src="./bootstrap/js/bootstrap.js"></script>
	<script src="./bootstrap/js/jquery-3.3.1.slim.min.js"></script>
  <style type="text/css">
    body{
      background-color: #0066ff;
      -webkit-animation: color 5s ease-in 0s infinite alternate running;
      -moz-animation: color 5s linear 0s infinite alternate running;
      animation: color 5s linear 0s infinite alternate running;
    }
    @-webkit-keyframes color{
        0%{background-color: #0066ff;}        
        32%{background-color:  purple;}
        55%{background-color: #0052cc;}
        76%{background-color: #0047b3;}
        100% {background-color: #003d99;}
      }
      @-moz-keyframes color{
        0%{background-color: #0066ff;} 
        32%{background-color:  purple;}
        55%{background-color: #0052cc;}
        76%{background-color: #0047b3;}
        100% {background-color: #003d99;}
      }
      @-webkit-keyframes color{
        0%{background-color: #0066ff;} 
        32%{background-color:  purple;}
        55%{background-color: #0052cc;}
        76%{background-color: #0047b3;}
        100% {background-color: #003d99;}
      }

  </style>
</head>
<body>
  <div class="container" style="margin-top: 100px;">
    <div class="row md-12">
      <div class="col md-4"></div>
      <div class="col md-4 p-4" style="border:1px solid #c0c0c0; border-radius: 5px; background-color: #c0c0c0; opacity: 0.8;">
<form action="" method="POST">
  <h1 class="text-center" style="margin-bottom:100px;">Register</h1>
  <div class="form-group">
    <input type="number" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="NIS" name="nis" required="required">
  </div>
  
  <div class="form-group">
    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password" required="required">
  </div>
  <input type="submit" class="btn btn-primary" style="width: 300px; height: 40px; font-size: 18px; margin-left: 20px;" name="submit" value="Submit">
    <div class="row md-12">
      <div class="col md-12 text-center mt-5">
        <a href="login.php">Sudah Punya Akun? Login</a>
      </div>
    </div>
</form>
      </div>
      <div class="col md-4"></div>
    </div>
  </div>
</body>
<?php
include 'koneksi.php';
  if (isset($_POST['submit'])) {
    $nis=$_POST['nis'];
    $password=$_POST['password'];

    $testsiswa=mysqli_query($conn,"SELECT * FROM siswa WHERE nis='$nis'");
    $testguru=mysqli_query($conn,"SELECT * FROM guru WHERE nip='$nis'");

    $datasiswa=mysqli_fetch_array($testsiswa);
    $dataguru=mysqli_fetch_array($testguru);

    $countsiswa=mysqli_num_rows($testsiswa); //nis
    $countguru=mysqli_num_rows($testguru);  //nis
    if ($countsiswa >0 || $countguru >0) {
      if ($datasiswa['password'] !=null || $dataguru['password']!=null) {
        ?>    
        <script type="text/javascript">
          alert("Anda Sudah Register");
        </script>
        <?php
      }else if($datasiswa['nis']!=null && empty($datasiswa['password'])){
    $update_siswa=mysqli_query($conn,"UPDATE siswa SET password='$password' WHERE nis='$nis'");
    if ($update_siswa) {
      ?>
      <script type="text/javascript">
        alert('Anda Berhasil Terdaftar!');
        location='./login.php';
      </script>
      <?php
    }else{
      ?>
      <script type="text/javascript">
        alert("Anda Gagal Terdaftar, masukkan kembali data diri anda!");
      </script>
      <?php
    }}else if($dataguru['nip']!=null &&empty($dataguru['password'])){

      $update_guru=mysqli_query($conn,"UPDATE guru SET password='$password' WHERE nip='$nis'");

    if ($update_guru) {
      ?>
      <script type="text/javascript">
        alert('Anda Berhasil Terdaftar Sebagai Guru!');        
        location='./loginguru.php';
      </script>

      <?php      
    }else{
      ?>
      <script type="text/javascript">
        alert("Anda Gagal Terdaftar, masukkan kembali data diri anda!");
      </script>
      <?php
    }
    }
    }else{
      ?>
      <script type="text/javascript">
        alert("Nis Tidak Terdaftar, Silahkan Masukan Nis anda dengan benar");
      </script>
      <?php
    }
    
  }
?>
</html>