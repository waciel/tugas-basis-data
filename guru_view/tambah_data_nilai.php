<?php
include '../koneksi.php';
session_start();
if(!isset($_SESSION['nip'])){
header('location:loginguru.php');
}else{
	$nis=$_GET['nis'];
	$nip=$_SESSION['nip'];
	$sql=mysqli_query($conn,"SELECT * FROM siswa INNER JOIN kelas ON siswa.idkelas=kelas.id WHERE siswa.nis='$nis'");
	$datasiswa=mysqli_fetch_array($sql);

	$sql2=mysqli_query($conn,"SELECT * FROM siswa WHERE nis='$nis'");
	$datasiswa2=mysqli_fetch_array($sql2);
	$idsiswa=$datasiswa2['id'];
	
	$guru=mysqli_query($conn,"SELECT * FROM guru WHERE nip='$nip'");
	$dg=mysqli_fetch_array($guru);
	$idguru=$dg['id'];
	$idkelas=$datasiswa['idkelas'];

	$cek=mysqli_query($conn,"SELECT * FROM mengajar WHERE idguru='$idguru' AND idkelas='$idkelas'");
	if($count=mysqli_num_rows($cek) ==0){
		?>
		<script>
			alert("Anda Tidak Mengajar Di Kelas Ini");
			location='nilai_siswa.php?nis=<?php echo $nis?>';
		</script>
		<?php
	}else{
?>
<!DOCTYPE html>
<html>
<head>
	<title>Tambah Data Nilai - SMKN 01 CIBINONG</title>
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/themify-icons.css">
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/font-awesome.min.css">
	<script src="../bootstrap/js/popper.min.js"></script>
	<script src="../bootstrap/js/bootstrap.min.js"></script>
	<script src="../bootstrap/js/bootstrap.js"></script>
	<script src="../bootstrap/js/jquery-3.3.1.slim.min.js"></script>
	<!-- cdn anjay -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<!-- //cdn -->
</head>
<body>
<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-dark bg-primary shadow fixed-top" style="background: linear-gradient(141deg, #9fb8ad 0%, #1fc8db 51%, #2cb5e8 10%);">
  <a class="navbar-brand" href="#"><img src="../image/logoteks.png" width="200px" height="80px"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse ml-5" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="home.php" style="font-size: 19px;">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item ">
        <a class="nav-link" href="data_siswa.php" style="font-size: 19px;">Data Siswa</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="data_guru.php" style="font-size: 19px;">Data Guru</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="data_nilai.php" style="font-size: 19px;">Data Nilai</a>
      </li>
    </ul>
    <ul class="navbar-nav flex-row ml-md-auto d-none d-md-flex">
	<div class="dropdown">
	  <button class="btn btn-transparent dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border:none; color: #fff; font-size: 18px;">
	    Hello, Wahyu Aditya Rizki!!
	  </button>
	  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
	    <a class="dropdown-item" href="#">Logout</a>
	    <a class="dropdown-item" href="#">Another action</a>
	    <a class="dropdown-item" href="#">Something else here</a>
	  </div>
	</div>
    </ul>
  </div>
</nav>
<!-- //Nabar END -->
<?php
	$nip=$_SESSION['nip'];
	$sql=mysqli_query($conn,"SELECT * FROM guru WHERE nip='$nip'");
	$dataguru=mysqli_fetch_array($sql);

	$idguru=$dataguru['id'];

	$mengajar=mysqli_query($conn,"SELECT * FROM mengajar INNER JOIN mapel ON mapel.id=mengajar.idmapel WHERE mengajar.idguru='$idguru'");
	$datamengajar=mysqli_fetch_array($mengajar);
?>
<div class="container" style="margin-top: 150px;">
	<div class="row-md-12">
		<a href="nilai_siswa.php?nis=<?php echo $nis?>" class="btn btn-lg btn-outline-dark">Back</a>
			<div class="col md-12"><h3>Tambah Data Nilai</h3></div>
		</div>
	<div class="row-md-12" style="padding:20px;">
		<div class="col md-12">
		<form action="" method="POST">
		  <div class="form-group">
			<label for="">Nis</label>
			<input type="text" name="nis" id="" class="form-control" value="<?php echo $datasiswa['nis']?>" aria-describedby="helpId" disabled>
		  </div>
		  <div class="form-group">
			<label for="">Nama Lengkap</label>
			<input type="text" name="nama" class="form-control" value="<?php echo $datasiswa['nama']?>" aria-describedby="helpId" disabled>
			
		  </div>
		  <div class="form-group">
			<label for="">Kelas</label>
			<input type="text" name="kelas" class="form-control" value="<?php echo $datasiswa['tingkat']." ". $datasiswa['idprodi']." ".$datasiswa['nomor']?>" aria-describedby="helpId" disabled>
		  </div>
		  <div class="form-group">
			<label for="">Nilai</label>
			<select name="nilai" class='form-control' required>
			<?php
			$idkelas=$datasiswa['idkelas'];
				$daftar_nilai=mysqli_query($conn,"SELECT * FROM daftar_nilai INNER JOIN mapel ON mapel.id=daftar_nilai.idmapel WHERE idguru='$idguru' AND idsiswa='$idsiswa'");

				$exist_id_mapel = array();

				while($dn=mysqli_fetch_array($daftar_nilai)){
					array_push($exist_id_mapel, $dn["idmapel"]);
				}

				$nilai=mysqli_query($conn,"SELECT * FROM mengajar INNER JOIN mapel ON mengajar.idmapel=mapel.id WHERE idguru='$idguru' AND idkelas='$idkelas'");
				if($count=mysqli_num_rows($nilai) >0){
					
					while($datanilai=mysqli_fetch_array($nilai)	){
						if(!in_array($dn["idmapel"], $exist_id_mapel)){
			?>
				<option  value="<?php echo $datanilai['id']; ?>" ><?php echo $datanilai['nama_mapel']; echo ((string)in_array($dn["idmapel"], $exist_id_mapel));?></option>
						<?php }}} ?>
			</select>
		  </div>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
					  <label for="">UH</label>
					  <input type="number" name="uh" class="form-control" placeholder="Nilai UH" aria-describedby="helpId" min="0" max="100" required>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
					  <label for="">UTS</label>
					  <input type="number" name="uts" class="form-control" placeholder="Nilai UTS" aria-describedby="helpId" min="0" max="100" required>					  
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
					  <label for="">UAS</label>
					  <input type="number" name="uas" class="form-control" placeholder="Nilai UAS" aria-describedby="helpId" min="0" max="100" required>
					</div>
				</div>
			</div>

			<button type="submit" class="btn btn-primary" name="simpan">Simpan</button>	
		  <button type="reset" class="btn btn-danger">Reset</button>
		</form>
		<?php
			if(isset($_POST['simpan'])){
			$idguru=$dataguru['id'];
			$idsiswa=$datasiswa2['id'];
			$idmapel=$_POST['nilai'];
			$uh=$_POST['uh'];
			$uts=$_POST['uts'];
			$uas=$_POST['uas'];
			
			
			 $insert=mysqli_query($conn,"INSERT INTO daftar_nilai(id_daftar_nilai,idguru,idsiswa,idmapel,uh,uts,uas) VALUES(null,'$idguru','$idsiswa','$idmapel','$uh','$uts','$uas')");			 
			 if($insert){
				?>
					<script>
						alert('Berhasil Memasukkan Data Nilai');
						location='nilai_siswa.php?nis=<?php echo $nis?>';
					</script>
				<?php
			 }else{
				?>
					<script>
						alert('Gagal Memasukkan Data Nilai');
						location='tambah_data_nilai.php?nis=<?php echo $nis?>';
					</script>
				<?php
			 }
			}
		?>
		</div>
	</div>
</div>
</body>
</html>
<?php
}
}
exit(0);

// test
if($idmapel ==$dn['idmapel']){
	?>
	<script>
		alert('Nilai <?php echo $dn['nama_mapel']?> sudah di masukkan');
		location='tambah_data_nilai.php?nis=<?php echo $nis?>';
	</script>
	<?php
}else{
}
?>
