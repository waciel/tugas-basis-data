<?php
	include '../koneksi.php';
	session_start();
	if (!isset($_SESSION['nip'])) {
		header("location:../loginguru.php");
	}else{
		$id=$_GET['nis'];
		$q=mysqli_query($conn,"SELECT * FROM siswa WHERE nis='$id'");
		$data=mysqli_fetch_array($q);
?>
<!DOCTYPE html>
<html>
<head>
	<title>Edit Siswa - SMKN 01 CIBINONG</title>
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/themify-icons.css">
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/font-awesome.min.css">
	<script src="../bootstrap/js/popper.min.js"></script>
	<script src="../bootstrap/js/bootstrap.min.js"></script>
	<script src="../bootstrap/js/bootstrap.js"></script>
	<script src="../bootstrap/js/jquery-3.3.1.slim.min.js"></script>
	<!-- cdn anjay -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<!-- //cdn -->
</head>
<body>
<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-dark bg-primary shadow fixed-top" style="background: linear-gradient(141deg, #9fb8ad 0%, #1fc8db 51%, #2cb5e8 10%);">
  <a class="navbar-brand" href="#">SMK Indonesia</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse ml-5" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="home.php" style="font-size: 19px;">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="#" style="font-size: 19px;">Data Siswa</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#" style="font-size: 19px;">Data Guru</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#" style="font-size: 19px;">Data Nilai</a>
      </li>
    </ul>
    <ul class="navbar-nav flex-row ml-md-auto d-none d-md-flex">
	<div class="dropdown">
	  <button class="btn btn-transparent dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border:none; color: #fff; font-size: 18px;">
	    Hello, <?php echo $_SESSION['nama']?>!!
	  </button>
	  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
	    <a class="dropdown-item" href="#">Logout</a>
	    <a class="dropdown-item" href="#">Another action</a>
	    <a class="dropdown-item" href="#">Something else here</a>
	  </div>
	</div>
    </ul>
  </div>
</nav>
<!-- //Nabar END -->
<div class="container" style="margin-top: 150px;">
	<div class="row-md-12">
		<a href="data_siswa.php" class="btn btn-lg btn-outline-dark">Back</a>
			<div class="col md-12"><h1>Edit Data Siswa</h1></div>
		</div>
	<div class="row-md-12" style="padding:20px;">
		<div class="col md-12">
		<form action="" method="POST">
		  <div class="form-group">
		    <label for="exampleInputEmail1">NIS</label>
		    <input type="number" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="NIS" name="nis" required="required" value="<?php echo $data['nis']?>" disabled>
		  </div>
		  <div class="form-group">
		    <label for="exampleInputPassword1">Nama Lengkap</label>
		    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Nama Lengkap" name="nama" required="required" value="<?php echo $data['nama']?>">
		  </div>
		  <div class="form-group">
		    <label for="exampleInputPassword1">Jenis Kelamin</label>
		    <select class="form-control" name="jk" required="required">
		    	<?php
		    	if ('Laki-Laki' ==$data['jeniskelamin']) {
		    		
		    			?>

						<option value="<?php echo $data['jeniskelamin']?>" selected><?php echo $data['jeniskelamin']?></option>
					<option value="Perempuan">Perempuan</option>
		    		<?php
		    	}else if('Perempuan' ==$data['jeniskelamin']){
		    		?>
					<option value="<?php echo $data['jeniskelamin']?>" selected><?php echo $data['jeniskelamin']?></option>
					<option value="Laki-Laki">Laki-Laki</option>

		    		<?php
		    	}else{
		    		echo "test";
		    	}
		    	?>
		    </select>
		  </div>
		  <div class="form-group">
		    <label for="exampleInputPassword1">Kelas</label>
		    <select class="form-control" name="kelas" required="required">
		    	<?php
		    	$qu=mysqli_query($conn,"SELECT * FROM kelas");
		    	$count=mysqli_num_rows($qu);
		    	if ($count >0) {
		    		while ($datas=mysqli_fetch_array($qu)){
		    		?>
		    			<option value="<?php echo $datas['id']?>"><?php echo $datas['tingkat']." ".$datas['idprodi']."".$datas['nomor']?></option>
		    		<?php if($data['idkelas'] == $datas['id']){ ?>
		    			<option value="<?php echo $datas['id']?>" selected><?php echo $datas['tingkat']." ".$datas['idprodi']."".$datas['nomor']?></option>
		    	<?php }}} ?>	
		    </select>
		  </div>
		  <input type="submit" class="btn btn-primary" name="update" value="Update">
		  <button type="reset" class="btn btn-danger">Reset</button>
		</form>
		</div>
		<div class="col md-12"></div>
	</div>
</div>
</body>
</html>
<?php
	if (isset($_POST['update'])) {
		$nama=$_POST['nama'];
		$jk=$_POST['jk'];
		$kelas=$_POST['kelas'];
		$insert=mysqli_query($conn,"UPDATE siswa SET nama='$nama',idkelas='$kelas',jeniskelamin='$jk' WHERE nis='$id'");

		if ($insert) {
			?>
		<script type="text/javascript">
			alert('Berhasil Menambahkan Data Siswa');
			location='data_siswa.php';
		</script>
			<?php
		}else{
			?>
		<script type="text/javascript">
			alert('Gagal Meambahkan Data Siswa');
		</script>
			<?php
		}
	}
}
?>