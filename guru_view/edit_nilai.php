<?php
	include '../koneksi.php';
	session_start();
	if (!isset($_SESSION['nip'])) {
		header("location:loginguru.php");
	}else{
        $nis=$_GET['nis'];
        
?>
<!DOCTYPE html>
<html>
<head>
	<title>Data Siswa - SMKKN 01 CIBINONG</title>
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/themify-icons.css">
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/font-awesome.min.css">
	<script src="../bootstrap/js/popper.min.js"></script>
	<script src="../bootstrap/js/bootstrap.min.js"></script>
	<script src="../bootstrap/js/bootstrap.js"></script>
	<script src="../bootstrap/js/jquery-3.3.1.slim.min.js"></script>
	<!-- cdn anjay -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<!-- //cdn -->
</head>
<body>
<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-dark bg-primary shadow fixed-top" style="background: linear-gradient(141deg, #9fb8ad 0%, #1fc8db 51%, #2cb5e8 10%);">
  <a class="navbar-brand" href="#">SMK Indonesia</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse ml-5" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="home.php" style="font-size: 19px;">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item ">
        <a class="nav-link" href="data_siswa.php" style="font-size: 19px;">Data Siswa</a>
      </li>
      <li class="nav-item " >
        <a class="nav-link" href="data_guru.php" style="font-size: 19px;">Data Guru</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="data_nilai.php" style="font-size: 19px;">Data Nilai</a>
      </li>
    </ul>
    <ul class="navbar-nav flex-row ml-md-auto d-none d-md-flex">
	<div class="dropdown">
	  <button class="btn btn-transparent dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border:none; color: #fff; font-size: 18px;">
	    Hello, <?php echo $_SESSION['nama']?>!!
	  </button>
	  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
	    <a class="dropdown-item" href="#">Logout</a>
	    <a class="dropdown-item" href="#">Another action</a>
	    <a class="dropdown-item" href="#">Something else here</a>
	  </div>
	</div>
    </ul>
  </div>
</nav>
<!-- //Nabar END -->
<!-- tabel -->
<?php 
    $nis=$_GET['nis'];        
    $id_nilai=$_GET['id_nilai'];
    $sqlnilai=mysqli_query($conn,"SELECT * FROM daftar_nilai INNER JOIN mapel ON mapel.id=daftar_nilai.idmapel WHERE id_daftar_nilai='$id_nilai'");
    $datanilai=mysqli_fetch_array($sqlnilai);

    $sql=mysqli_query($conn,"SELECT * FROM siswa WHERE nis='$nis'");
    $siswa=mysqli_fetch_array($sql);
    $id_kelas=$siswa['idkelas'];
    $id_siswa=$siswa['id'];

    $datakelas=mysqli_query($conn,"SELECT * FROM kelas JOIN prodi ON kelas.idprodi=prodi.id_prodi WHERE kelas.id='$id_kelas'");
    
    $dd=mysqli_fetch_array($datakelas);

?>
<div class="container" style="margin-top: 200px;">
	<!-- <a href="tambah_data_nilai.php" class="btn btn-success">Tambah Data kelas</a><br><br> -->
    <a href="nilai_siswa.php?nis=<?php echo $nis?>" class="btn btn-secondary"><i class="fa fa-backward" aria-hidden="true"></i> Back</a><br><br>

        <form action="" method="POST">
            <div class="form-group">
              <label for="">Nis</label>
              <input type="text" class="form-control" name="nis" value="<?php echo $siswa['nis']?>" disabled>
            </div>
            <div class="form-group">
              <label for="">Nama Lengkap</label>
              <input type="text" class="form-control" name="" id="" value="<?php echo $siswa['nama']?>" placeholder="Nama Siswa" disabled>
            </div>
            <div class="form-group">
              <label for="">Kelas</label>
              <input type="text" class="form-control" name="" id="" value="<?php echo $dd['tingkat']." ".$dd['idprodi']." ".$dd['nomor']?>" aria-describedby="helpId" disabled>
            </div>
            <div class="row">
                <div class="col-md-6">
                <h6>Nilai <?php echo $datanilai['nama_mapel']?></h6>
                </div>
                <div class="col-md-6">
                <h6 class="text-center"> <?php
                    echo "Guru : ". $_SESSION['nama'];
                ?>
                </h6>
                </div>
            </div>
            <div class="row">
            <div class="col-md-3"></div>
                <div class="col-md-2">
                    <div class="form-group">
                      <label for=""></label>
                      <input type="number" max="100" min="0"
                        class="form-control text-center" name="uh"  max="100" id="" aria-describedby="helpId" value="<?php echo $datanilai['uh']?>" placeholder="Nilai UH">
                      <small id="helpId" class="form-text text-muted text-center"><h5>UH</h5></small>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                      <label for=""></label>
                      <input type="number"
                        class="form-control text-center" name="uts" id="" min="0" max="100" aria-describedby="helpId" value="<?php echo $datanilai['uts']?>" placeholder="Nilai UTS">
                      <small id="helpId" class="form-text text-muted text-center"><H5>UTS</H5></small>
                    </div>
                </div><div class="col-md-2">
                    <div class="form-group">
                      <label for=""></label>
                      <input type="number"
                        class="form-control text-center" name="uas" id="" min="0" max="100" aria-describedby="helpId" value="<?php echo $datanilai['uas']?>" placeholder="Nilai UAS">
                      <small id="helpId" class="form-text text-muted text-center"><H5>UAS</H5></small>
                    </div>
                </div>
            <div class="col-md-3"></div>
            </div>
            <input type="submit" name="edit" class="btn btn-success btn-block mb-3" value="Edit" onClick="return confirm('Apakah Anda Yakin Ingin Mengedit Data?')">
            <input type="reset" class="btn btn-danger btn-block mb-3" value="Reset">
        </form>
        <?php
          if(isset($_POST['edit'])){
            $uh=$_POST['uh'];
            $uts=$_POST['uts'];
            $uas=$_POST['uas'];

            $update=mysqli_query($conn,"UPDATE daftar_nilai SET uh='$uh',uts='$uts',uas='$uas' WHERE id_daftar_nilai='$id_nilai'");
            if($update){
              ?>
                <script>
                  alert("Berhasil Mengupdate Nilai");
                  location='nilai_siswa.php?nis=<?php echo $nis?>';
                </script>
              <?php
            }else{
              ?>
              <script>
                alert("Gagal Mengupdate Nilai");
                location='edit_nilai.php?nis=<?php echo $nis?>&id_nilai=<?php echo $id_nilai?>';
              </script>
              <?php
            }
          }
        ?>
	
</div>
<!-- /edtable -->
</body>
</html>
<?php }?>