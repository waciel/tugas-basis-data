<?php
	include '../koneksi.php';
	session_start();
	if (!isset($_SESSION['nip'])) {
		header("location:loginguru.php");
	}else{
        
?>
<!DOCTYPE html>
<html>
<head>
	<title>Data Siswa - SMKKN 01 CIBINONG</title>
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/themify-icons.css">
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/font-awesome.min.css">
	<script src="../bootstrap/js/popper.min.js"></script>
	<script src="../bootstrap/js/bootstrap.min.js"></script>
	<script src="../bootstrap/js/bootstrap.js"></script>
	<script src="../bootstrap/js/jquery-3.3.1.slim.min.js"></script>
	<!-- cdn anjay -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<!-- //cdn -->
</head>
<body>
<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-dark bg-primary shadow fixed-top" style="background: linear-gradient(141deg, #9fb8ad 0%, #1fc8db 51%, #2cb5e8 10%);">
  <a class="navbar-brand" href="#">SMK Indonesia</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse ml-5" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="home.php" style="font-size: 19px;">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item ">
        <a class="nav-link" href="data_siswa.php" style="font-size: 19px;">Data Siswa</a>
      </li>
      <li class="nav-item " >
        <a class="nav-link" href="data_guru.php" style="font-size: 19px;">Data Guru</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="data_nilai.php" style="font-size: 19px;">Data Nilai</a>
      </li>
    </ul>
    <ul class="navbar-nav flex-row ml-md-auto d-none d-md-flex">
	<div class="dropdown">
	  <button class="btn btn-transparent dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border:none; color: #fff; font-size: 18px;">
	    Hello, <?php echo $_SESSION['nama']?>!!
	  </button>
	  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
	    <a class="dropdown-item" href="#">Logout</a>
	    <a class="dropdown-item" href="#">Another action</a>
	    <a class="dropdown-item" href="#">Something else here</a>
	  </div>
	</div>
    </ul>
  </div>
</nav>
<!-- //Nabar END -->
<!-- tabel -->
<?php 
    $id_kelas=$_GET['id_kelas'];
      $datakelas=mysqli_query($conn,"SELECT * FROM kelas JOIN prodi ON kelas.idprodi=prodi.id WHERE kelas.id='$id_kelas'");
    $dd=mysqli_fetch_array($datakelas);
    // mysqli_result Object ( [current_field] => 0 [field_count] => 6 [lengths] => [num_rows] => 0 [type] => 0 )


?>
<div class="container" style="margin-top: 200px;">
	<!-- <a href="tambah_data_nilai.php" class="btn btn-success">Tambah Data kelas</a><br><br> -->
    <a href="data_nilai_kelas.php?id_prodi=<?php echo $dd['idprodi']?>" class="btn btn-secondary"><i class="fa fa-backward" aria-hidden="true"></i> Back</a><br><br>
    <h3 class="text-center"><?php echo $dd['tingkat']." ".$dd['idprodi']." ".$dd['nomor']?></h3>
<table class="table table-hover table-stripped">
	<thead class="thead-dark">
		<tr>
		<th>No</th>
        <th>Mapel</th>
		<th>Option</th>
	</tr>
	</thead>
	<tbody>
	<?php
	$data_siswa=mysqli_query($conn,"SELECT * FROM mapel");
	$no=1;
	$count=mysqli_num_rows($data_siswa);
	if( $count >0){
		while($datas=mysqli_fetch_array($data_siswa)){
	?>
	<tr>
		<td><?php echo $no++ ?></td>		
        <td><?php echo $datas['nama_mapel']?></td>
		<td><a href="data_nilai_kelas_siswa.php?id_mapel=<?php echo $datas['id'];?>&&id_kelas=<?php echo $id_kelas?>" class="btn btn-info" href="#" role="button"> <i class="fa fa-eye" aria-hidden="true" title="Lihat Nilai <?php echo $datas['nama']?>"></i>   </a><!---&nbsp;|&nbsp;--->
		<!-- <a class="btn btn-danger" href="delete_kelas.php?id=<?php echo $datak['id']?>" title="Delete"><i class="fa fa-trash"></i></a> 
		<a name="" id="" class="btn btn-primary" href="" title="edit"><i class="fa fa-edit    "></i></a> -->
		</td>
	</tr>
	<?php
		}}else{
			?>
				<tr>
				<td colspan="5" class="text-center"><h3>Data Kosong</h3></td>
				</tr>
			<?php
		}
	?>
	</tbody>
</table>
</div>
<!-- /edtable -->
</body>
</html>
<?php }?>