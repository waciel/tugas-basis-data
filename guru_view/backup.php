<?php
	include '../koneksi.php';
	session_start();
	if (!isset($_SESSION['nip'])) {
		header("location:loginguru.php");
	}else{
        
?>
<!DOCTYPE html>
<html>
<head>
	<title>Data Siswa - SMKKN 01 CIBINONG</title>
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/themify-icons.css">
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/font-awesome.min.css">
	<script src="../bootstrap/js/popper.min.js"></script>
	<script src="../bootstrap/js/bootstrap.min.js"></script>
	<script src="../bootstrap/js/bootstrap.js"></script>
	<script src="../bootstrap/js/jquery-3.3.1.slim.min.js"></script>
	<!-- cdn anjay -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<!-- //cdn -->
</head>
<body>
<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-dark bg-primary shadow fixed-top" style="background: linear-gradient(141deg, #9fb8ad 0%, #1fc8db 51%, #2cb5e8 10%);">
  <a class="navbar-brand" href="#">SMK Indonesia</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse ml-5" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="home.php" style="font-size: 19px;">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item ">
        <a class="nav-link" href="data_siswa.php" style="font-size: 19px;">Data Siswa</a>
      </li>
      <li class="nav-item " >
        <a class="nav-link" href="data_guru.php" style="font-size: 19px;">Data Guru</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="data_nilai.php" style="font-size: 19px;">Data Nilai</a>
      </li>
    </ul>
    <ul class="navbar-nav flex-row ml-md-auto d-none d-md-flex">
	<div class="dropdown">
	  <button class="btn btn-transparent dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border:none; color: #fff; font-size: 18px;">
	    Hello, <?php echo $_SESSION['nama']?>!!
	  </button>
	  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
	    <a class="dropdown-item" href="#">Logout</a>
	    <a class="dropdown-item" href="#">Another action</a>
	    <a class="dropdown-item" href="#">Something else here</a>
	  </div>
	</div>
    </ul>
  </div>
</nav>
<!-- //Nabar END -->
<!-- tabel -->
<?php 
	$id_mapel=$_GET['id_mapel'];
	$id_kelas=$_GET['id_kelas'];
	$sql=mysqli_query($conn,"SELECT * FROM daftar_nilai JOIN siswa ON daftar_nilai.idsiswa=siswa.id WHERE daftar_nilai.idmapel='$id_mapel' && siswa.idkelas='$id_kelas'");
	$nilai=mysqli_fetch_array($sql);

    $datakelas=mysqli_query($conn,"SELECT * FROM kelas JOIN prodi ON kelas.idprodi=prodi.id WHERE kelas.id='$id_kelas'");
    
    $dd=mysqli_fetch_array($datakelas);

?>
<div class="container" style="margin-top: 200px;">
	<!-- <a href="tambah_data_nilai.php" class="btn btn-success">Tambah Data kelas</a><br><br> -->
    <a href="data_nilai_kelas.php?id_prodi=<?php echo $dd['idprodi']?>" class="btn btn-secondary"><i class="fa fa-backward" aria-hidden="true"></i> Back</a><br><br>
	<div class="row">
	<div class="col-md-12">
	<h3 class="text-center"><?php echo $dd['tingkat']." ".$dd['idprodi']." ".$dd['nomor']?></h3>
	</div>
	</div>
<table class="table table-hover table-stripped">
	<thead class="thead-dark">
		<tr>
		<th>No</th>
        <th>Nis</th>
        <th>Nama Lengkap</th>
        <th>Jenis Kelamin</th>
		<th>Tugas</th>
		<TH>UH</TH>
		<th>UTS</th>
		<th>UAS</th>
		<th>Option</th>
	</tr>
	</thead>
	<tbody>
	<?php
    $id_kelas=$_GET['id_kelas'];
	$data_siswa=mysqli_query($conn,"SELECT * FROM siswa JOIN daftar_nilai ON daftar_nilai.idsiswa=siswa.id WHERE siswa.idkelas='$id_kelas' && daftar_nilai.idmapel='$id_mapel' ");
	$no=1;
	$count=mysqli_num_rows($data_siswa);
	if( $count >0){
		$siswa=mysqli_query($conn,"SELECT * FROM siswa JOIN daftar_nilai ON daftar_nilai.idsiswa=siswa.id WHERE siswa.idkelas='$id_kelas' && daftar_nilai.idmapel='$id_mapel'");
		$datasiswa=mysqli_fetch_array($siswa);
		$nis=$datasiswa['nis'];
			$carisiswa=mysqli_query($conn,"SELECT * FROM siswa WHERE nis='$nis'");
			$s=mysqli_fetch_array($carisiswa);
			$idsiswa=$s['id'];
			$sql=mysqli_query($conn,"SELECT * FROM daftar_nilai WHERE idsiswa='$idsiswa'");
			$tes=mysqli_fetch_array($sql);
			print_r($tes);
			$cont=mysqli_num_rows($sql);
			if(!$cont >0){
		while($datas=mysqli_fetch_array($data_siswa)){
			
	?>
	<!-- jika tidak ada idsiswa yang sama di tabel daftar_nilai -->
	<tr>
		<td><?php echo $no++ ?></td>		
        <td><?php echo $datas['nis']?></td>
        <td><?php echo $datas['nama']?></td>
        <td><?php echo $datas['jeniskelamin']?></td>
		<td>
		<?php
			$carisiswa=mysqli_query($conn,"SELECT * FROM siswa WHERE nis='$nis'");
			$s=mysqli_fetch_array($carisiswa);
			$idsiswa=$s['id'];
			$sql=mysqli_query($conn,"SELECT * FROM daftar_nilai WHERE idnilai='4' AND idsiswa='$idsiswa'");
			$count=mysqli_num_rows($sql);
			$data=mysqli_fetch_array($sql);
				if($data['nilai'] ==""){
					echo "0";
					
				}else{
					echo $data['nilai'];
				}
			?>
		</td>
		<td>
			<?php
			$carisiswa=mysqli_query($conn,"SELECT * FROM siswa WHERE nis='$nis'");
			$s=mysqli_fetch_array($carisiswa);
			$idsiswa=$s['id'];
			$sql=mysqli_query($conn,"SELECT * FROM daftar_nilai WHERE idnilai='1' AND idsiswa='$idsiswa'");
			$count=mysqli_num_rows($sql);
			$data=mysqli_fetch_array($sql);
				if($data['nilai'] ==""){
					echo "0";
					
				}else{
					echo $data['nilai'];
				}
			?>
		</td>
		<td>
				<?php
				$carisiswa=mysqli_query($conn,"SELECT * FROM siswa WHERE nis='$nis'");
				$s=mysqli_fetch_array($carisiswa);
				$idsiswa=$s['id'];
				$sql=mysqli_query($conn,"SELECT * FROM daftar_nilai WHERE idnilai='2' AND idsiswa='$idsiswa'");
				$count=mysqli_num_rows($sql);
				$data=mysqli_fetch_array($sql);
					if($data['nilai'] ==""){
						echo "0";
					}else{
						echo $data['nilai'];
					}
				?>
		</td>
		<td>
			<?php
			$carisiswa=mysqli_query($conn,"SELECT * FROM siswa WHERE nis='$nis'");
			$s=mysqli_fetch_array($carisiswa);
			$idsiswa=$s['id'];
			$sql=mysqli_query($conn,"SELECT * FROM daftar_nilai WHERE idnilai='3' AND idsiswa='$idsiswa'");
			$count=mysqli_num_rows($sql);
			$data=mysqli_fetch_array($sql);
				if($data['nilai'] ==""){
					echo "0";
				}else{
					echo $data['nilai'];
				}
			?>
		</td>
		<td><a href="pilih_nilai.php?nis=<?php echo $datas['nis']?>" class="btn btn-info" href="#" role="button"> <i class="fa fa-eye" aria-hidden="true" title="Lihat Nilai <?php echo $datas['nama']?>"></i>   </a><!---&nbsp;|&nbsp;--->
		<!-- <a class="btn btn-danger" href="delete_kelas.php?id=<?php echo $datak['id']?>" title="Delete"><i class="fa fa-trash"></i></a> 
		<a name="" id="" class="btn btn-primary" href="" title="edit"><i class="fa fa-edit    "></i></a> -->
		</td>
	</tr>
	<!-- end -->
	<?php	
		}}else{
			$datas=mysqli_fetch_array($data_siswa);
	?>
		<tr>
		<td><?php echo $no++ ?></td>		
        <td><?php echo $datas['nis']?></td>
        <td><?php echo $datas['nama']?></td>
        <td><?php echo $datas['jeniskelamin']?></td>
		<td>
		<?php
			$carisiswa=mysqli_query($conn,"SELECT * FROM siswa WHERE nis='$nis'");
			$s=mysqli_fetch_array($carisiswa);
			$idsiswa=$s['id'];
			$sql=mysqli_query($conn,"SELECT * FROM daftar_nilai WHERE idnilai='4' AND idsiswa='$idsiswa'");
			$count=mysqli_num_rows($sql);
			$data=mysqli_fetch_array($sql);
				if($data['nilai'] ==""){
					echo "0";
					
				}else{
					echo $data['nilai'];
				}
			?>
		</td>
		<td>
			<?php
			$carisiswa=mysqli_query($conn,"SELECT * FROM siswa WHERE nis='$nis'");
			$s=mysqli_fetch_array($carisiswa);
			$idsiswa=$s['id'];
			$sql=mysqli_query($conn,"SELECT * FROM daftar_nilai WHERE idnilai='1' AND idsiswa='$idsiswa'");
			$count=mysqli_num_rows($sql);
			$data=mysqli_fetch_array($sql);
				if($data['nilai'] ==""){
					echo "0";
					
				}else{
					echo $data['nilai'];
				}
			?>
		</td>
		<td>
				<?php
				$carisiswa=mysqli_query($conn,"SELECT * FROM siswa WHERE nis='$nis'");
				$s=mysqli_fetch_array($carisiswa);
				$idsiswa=$s['id'];
				$sql=mysqli_query($conn,"SELECT * FROM daftar_nilai WHERE idnilai='2' AND idsiswa='$idsiswa'");
				$count=mysqli_num_rows($sql);
				$data=mysqli_fetch_array($sql);
					if($data['nilai'] ==""){
						echo "0";
					}else{
						echo $data['nilai'];
					}
				?>
		</td>
		<td>
			<?php
			$carisiswa=mysqli_query($conn,"SELECT * FROM siswa WHERE nis='$nis'");
			$s=mysqli_fetch_array($carisiswa);
			$idsiswa=$s['id'];
			$sql=mysqli_query($conn,"SELECT * FROM daftar_nilai WHERE idnilai='3' AND idsiswa='$idsiswa'");
			$count=mysqli_num_rows($sql);
			$data=mysqli_fetch_array($sql);
				if($data['nilai'] ==""){
					echo "0";
				}else{
					echo $data['nilai'];
				}
			?>
		</td>
		<td><a href="pilih_nilai.php?nis=<?php echo $datas['nis']?>" class="btn btn-info" href="#" role="button"> <i class="fa fa-eye" aria-hidden="true" title="Lihat Nilai <?php echo $datas['nama']?>"></i>   </a><!---&nbsp;|&nbsp;--->
		<!-- <a class="btn btn-danger" href="delete_kelas.php?id=<?php echo $datak['id']?>" title="Delete"><i class="fa fa-trash"></i></a> 
		<a name="" id="" class="btn btn-primary" href="" title="edit"><i class="fa fa-edit    "></i></a> -->
		</td>
	</tr>
	<?php		
		}}else{
			?>
				<tr>
				<td colspan="9" class="text-center"><h3>Data Kosong</h3></td>
				</tr>
			<?php
		}
	?>
	</tbody>
</table>
</div>
<!-- /edtable -->
</body>
</html>
<?php }?>