<?php
	include '../koneksi.php';
	session_start();
	if (!isset($_SESSION['nip'])) {
		header("location:loginguru.php");
	}else{
?>
<!DOCTYPE html>
<html>
<head>
	<title>Home -  SMKN 01 CIBINONG</title>
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/themify-icons.css">
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/font-awesome.min.css">
	<script src="../bootstrap/js/popper.min.js"></script>
	<script src="../bootstrap/js/bootstrap.min.js"></script>
	<script src="../bootstrap/js/bootstrap.js"></script>
	<script src="../bootstrap/js/jquery-3.3.1.slim.min.js"></script>
	<!-- cdn anjay -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<!-- //cdn -->
<style type="text/css">
	.card{
	transition: .4s ease-in-out;
	}
	.card:hover{
	transform: scale(1.1);
	transition: .4s ease-in-out;
	box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
	}
	.quotes{
	transition: .4s ease-in-out;
	}
	.quotes:hover{
	color: white;
	transition: .4s ease-in-out;		
	}
</style>
</head>
<body>
	<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-dark bg-primary shadow fixed-top" style="background: linear-gradient(141deg, #9fb8ad 0%, #1fc8db 51%, #2cb5e8 10%);">
  <a class="navbar-brand" href="#">SMK Indonesia</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse ml-5" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="home.php" style="font-size: 19px;">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="data_siswa.php" style="font-size: 19px;">Data Siswa</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="data_guru.php" style="font-size: 19px;">Data Guru</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="data_nilai.php" style="font-size: 19px;">Data Nilai</a>
      </li>
    </ul>
    <ul class="navbar-nav flex-row ml-md-auto d-none d-md-flex">
	<div class="dropdown">
	  <button class="btn btn-transparent dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border:none; color: #fff; font-size: 18px;">
	    Hello, <?php echo $_SESSION['nama']?>!!
	  </button>
	  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
	    <a class="dropdown-item" href="logout.php" onclick="return confirm('Are You Sure?');">Logout</a>
	    <a class="dropdown-item" href="#">Another action</a>
	    <a class="dropdown-item" href="#">Something else here</a>
	  </div>
	</div>
    </ul>
  </div>
</nav>
<!-- //Nabar END -->
<!-- carrousel -->
<div id="carouselExampleSlidesOnly mt-5" class="carousel slide" data-ride="carousel" style="margin-top: 100px;">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="../image/jempol.png" alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="../image/smkbisa.png" alt="Second slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="..." alt="Third slide">
    </div>
  </div>
</div>
<!-- //carrousel end-->
<div class="jumbotron jumbotron-fluid " style="background: linear-gradient(141deg, #9fb8ad 0%, #1fc8db 51%, #fff 50%); height: auto;">
	<h1 class="text-center" style="color:#fff;">Keunggulan</h1><Br><br><br>
	<!-- card keungg -->
	<div class="container">
	<div class="row md-12">
	<div class="col md-4 d-flex justify-content-center" style="background: ">
	<div class="card" style="width:21rem; border:none;">
	  <img class="card-img-top" src="../image/kejuruan.jpg" alt="Card image cap">
	  <div class="card-body">
	    <p class="card-text text-center" style="font-weight: bold; font-size: 20px; font-family: arial;">Memiliki 10 Jurusan</p>
	  </div>
	</div>
		</div>
	<div class="col md-4 d-flex justify-content-center">
	  <div class="card" style="width:21rem; border:none;">
	  <img class="card-img-top" src="../image/gurus.jpg" alt="Card image cap">
	  <div class="card-body">
	    <p class="card-text text-center" style="font-weight: bold; font-size: 20px; font-family: arial;">Memiliki Guru-Guru Yang Kompeten Dalam Bidangnya</p>
	  </div>
	</div> 
		</div>
		<div class="col md-4 d-flex justify-content-center">
	<div class="card" style="width:21rem; border:none;">
	  <img class="card-img-top" src="../image/fasili.png" alt="Card image cap">
	  <div class="card-body">
	    <p class="card-text text-center" style="font-weight: bold; font-size: 20px; font-family: arial;">Memiliki Fasilitas Yang Memadai</p>
	  </div>
	</div>			
		</div>	
	</div>
	  <!-- //card keung -->
	</div>
	</div>
	<!-- //jumbotron fluidkeung -->
	<!-- jumbotron fluidquotes -->
	<div class="jumbotron jumbotron-fluid quotes" style="background-image: url(../image/book.jpg); background-size: cover; background-attachment: fixed; height: 50vh;">
		<h1 class="text-center" style="margin-top: 100px;"><q><i>Teruslah Belajar Walaupun Sampai Ke Negeri Cina</i></q></h1>
	</div>
	<div class="jumbotron jumbotron-fluid" style="background: transparent;">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3963.99812389225!2d106.80583161449604!3d-6.521917965573281!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69c3db9bbedcc3%3A0x1f5280e86053b1e9!2sSMK%20NEGERI%201%20Cibinong!5e0!3m2!1sid!2sid!4v1567226064773!5m2!1sid!2sid" width="100%" height="600" frameborder="0" style="border:0;"></iframe>
	</div>
</div>


</body>
<script type="text/javascript">
	$('.carousel').carousel()
</script>
</html>
<?php }?>