<?php
	include '../koneksi.php';
	session_start();
	if (!isset($_SESSION['nip'])) {
		header("location:loginguru.php");
	}else{
        $nis=$_GET['nis'];
        
?>
<!DOCTYPE html>
<html>
<head>
	<title>Data Siswa - SMKKN 01 CIBINONG</title>
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/themify-icons.css">
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/font-awesome.min.css">
	<script src="../bootstrap/js/popper.min.js"></script>
	<script src="../bootstrap/js/bootstrap.min.js"></script>
	<script src="../bootstrap/js/bootstrap.js"></script>
	<script src="../bootstrap/js/jquery-3.3.1.slim.min.js"></script>
	<!-- cdn anjay -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<!-- //cdn -->
</head>
<body>
<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-dark bg-primary shadow fixed-top" style="background: linear-gradient(141deg, #9fb8ad 0%, #1fc8db 51%, #2cb5e8 10%);">
  <a class="navbar-brand" href="#">SMK Indonesia</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse ml-5" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="home.php" style="font-size: 19px;">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item ">
        <a class="nav-link" href="data_siswa.php" style="font-size: 19px;">Data Siswa</a>
      </li>
      <li class="nav-item " >
        <a class="nav-link" href="data_guru.php" style="font-size: 19px;">Data Guru</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="data_nilai.php" style="font-size: 19px;">Data Nilai</a>
      </li>
    </ul>
    <ul class="navbar-nav flex-row ml-md-auto d-none d-md-flex">
	<div class="dropdown">
	  <button class="btn btn-transparent dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border:none; color: #fff; font-size: 18px;">
	    Hello, <?php echo $_SESSION['nama']?>!!
	  </button>
	  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
	    <a class="dropdown-item" href="#">Logout</a>
	    <a class="dropdown-item" href="#">Another action</a>
	    <a class="dropdown-item" href="#">Something else here</a>
	  </div>
	</div>
    </ul>
  </div>
</nav>
<!-- //Nabar END -->
<!-- tabel -->
<?php 
    $sql=mysqli_query($conn,"SELECT * FROM siswa WHERE nis='$nis'");
    $siswa=mysqli_fetch_array($sql);
    $id_kelas=$siswa['idkelas'];
    $id_siswa=$siswa['id'];

    $datakelas=mysqli_query($conn,"SELECT * FROM kelas JOIN prodi ON kelas.idprodi=prodi.id_prodi WHERE kelas.id='$id_kelas'");
    
    $dd=mysqli_fetch_array($datakelas);

?>
<div class="container" style="margin-top: 200px;">
	<!-- <a href="tambah_data_nilai.php" class="btn btn-success">Tambah Data kelas</a><br><br> -->
    <a href="data_nilai_kelas_siswa.php?id_kelas=<?php echo $dd['id']?>" class="btn btn-secondary"><i class="fa fa-backward" aria-hidden="true"></i> Back</a>
    <a class="btn btn-info" href="tambah_data_nilai.php?nis=<?php echo $nis?>" role="button" title="Tambah Data Nilai"> <i class="fa fa-book" aria-hidden="true"></i></a>
    <br><br>
	<div class="row">
    <div class="col-md-4">
    <h5>Nis : <?php echo $siswa['nis']?></h5>
    </div>
	<div class="col-md-4">
    <h5>Nama Siswa : <?php echo $siswa['nama']?></h5>
	</div>
    <div class="col-md-4">
    <h5>Kelas : <?php echo $dd['tingkat']." ".$dd['idprodi']." ".$dd['nomor']?></h5>
    </div>
	</div><br>
    <!-- mtk -->
    <table class="table table-hover">
        <thead>
            <tr>
                <th rowspan="2"><p style="margin-bottom:10%;">Mata Pelajaran</p></th>
                <th colspan="4" class="text-center">Nilai</th>
                <th rowspan="2" class="text-center"><p style="margin-bottom:10%;">Nama Guru</p></th>
                <th rowspan="2" class="text-center"><p style="margin-bottom:10%;">Action</p></th>
            </tr>
            <tr class="text-center">
                <th>UH</th>
                <th>UTS</th>
                <th>UAS</th>
                <th>Nilai Akhir</th>
            </tr>
        </thead>
        <tbody>
        <?php
        $nis=$_GET['nis'];
        $sqlsiswa=mysqli_query($conn,"SELECT * FROM siswa WHERE nis='$nis'");
        $datasiswa=mysqli_fetch_array($sqlsiswa);
        $id_siswa=$datasiswa['id'];
            $sql=mysqli_query($conn,"SELECT * FROM daftar_nilai INNER JOIN mapel ON daftar_nilai.idmapel=mapel.id INNER JOIN guru ON guru.id=daftar_nilai.idguru WHERE idsiswa='$id_siswa' ORDER BY mapel.nama_mapel ASC");
            if($count=mysqli_num_rows($sql) >0){
                $query_mengajar = "SELECT * FROM mengajar INNER JOIN mapel ON mapel.id=mengajar.idmapel INNER JOIN guru ON guru.id=mengajar.idguru INNER JOIN kelas ON kelas.id=mengajar.idkelas WHERE mengajar.idkelas='$id_kelas'";
                $mengajar=mysqli_query($conn,$query_mengajar);
                // echo $query_mengajar;
                $nip=$_SESSION['nip'];
                $id_kelas=$dd['id'];

                //ambil data untuk guru yang sekarang login
                $guru=mysqli_query($conn,"SELECT * FROM guru WHERE nip='$nip'");
                $dataguru=mysqli_fetch_array($guru);
                $idguru=$dataguru['id'];

                while($datamengajar=mysqli_fetch_array($mengajar) && $data=mysqli_fetch_array($sql)){
                    
                    
        ?>
            <tr title="<?php 
                $uh=$data['uh'];
                $uts=$data['uts'];
                $uas=$data['uas'];
                    if($uh =="0"){
                        echo "Harap Isi Nilai Ulangan Harian";
                    }else if($uts =="0"){
                        echo "Harap Isi Nilai Ulangan Tengah Semester";
                    }else if($uas =="0"){
                        echo "Harap Isi Nilai Ulangan Akhir Semester";
                    }
                ?>">
                <td><?php echo $data['nama_mapel']?></td>
                <Td class="text-center"><?php echo $data['uh']?></Td>
                <td class="text-center"><?php echo $data['uts']?></td>
                <td class="text-center"><?php echo $data['uas']?></td>
                <td >
                    <?php
                        
                        $akhir=($uh+$uts+$uas)/3;
                        if($uh =="" || $uts=="" || $uas=="" || $uh =="0" || $uts=="0" || $uas=="0"){
                            echo "<p class='text-center'><b>-</b></p>";                            
                        }else{
                            echo "<p class='text-center'><b>".$akhir."</b></p>";
                        }
                    ?>
                </td>
                <td class="text-center"><?php echo $data['nama']?></td>
                <td> 
                        <?php
                        // echo($data["idguru"]);
                        //if(($datamengajar['nama_mapel'] == $data['nama_mapel'])){
                        if ($data["idguru"]==$idguru){
                                ?>
                                    <a class="btn btn-primary" href="edit_nilai.php?nis=<?php echo $siswa['nis']?>&id_nilai=<?php echo $data['id_daftar_nilai']?>" role="button"><i class="fa fa-edit" title="Edit Nilai"></i></a>
                                    <a class="btn btn-outline-danger" href="delete_nilai.php?nis=<?php echo $siswa['nis']?>&id_nilai=<?php echo $data['id_daftar_nilai']?>" role="button" title="Hapus Nilai" onClick="return confirm('Apakah Anda Yakin Ingin Menghapus Data Nilai?')"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                <?php
                            }else{
                            
                            }
                        ?>
                
                </td>
            </tr>
            <?php }}else{?>
            <tr>
                <td colspan="9"><h5 class="text-center">DATA KOSONG</h5></td>
            </tr>
            <?php }?>
        </tbody>
    </table>
    <?php exit(0); ?>
<table class="table table-hover table-stripped">
	<thead class="thead-dark">
        <tr>
        <th colspan="5" class="text-center">MATEMATIKA</th>
        </tr>
		<tr class="text-center">
        <th>Tugas</th>
        <th>UH</th>
        <th>UTS</th>
		<th>UAS</th>
        <th>Nilai Akhir</th>
	</tr>
	</thead>
	<tbody>
	<?php
	$data_siswa=mysqli_query($conn,"SELECT * FROM daftar_nilai WHERE idsiswa='$id_siswa' AND idmapel='1' ");
	$no=1;
	$count=mysqli_num_rows($data_siswa);
	if( $count >0){
		$datas=mysqli_fetch_array($data_siswa);
			
	?>
	<tr>
        <td>
        <?php 
        $sqlnilai=mysqli_query($conn,"SELECT *FROM daftar_nilai WHERE idsiswa='$id_siswa' AND idmapel='1' AND idnilai='4'");
        $tugas=mysqli_fetch_array($sqlnilai);
        if($count=mysqli_num_rows($sqlnilai)){
        ?>
        <div class="text-center">
        <?php echo $tugas['nilai'];?>
        </div>
        <?php }else{
            ?>
            <div class="text-center">0</div>
        <?php }?>
        </td>
        <td>
        <?php 
        $sqlnilai=mysqli_query($conn,"SELECT *FROM daftar_nilai WHERE idsiswa='$id_siswa' AND idmapel='1' AND idnilai='1'");
        $uh=mysqli_fetch_array($sqlnilai);
        if($count=mysqli_num_rows($sqlnilai)){
        ?>
        <div class="text-center">
        <?php echo $uh['nilai'];?>
        </div>
        <?php }else{
            ?>
            <div class="text-center">0</div>
        <?php }?>
        </td>
        <td>
        <?php 
        $sqlnilai=mysqli_query($conn,"SELECT *FROM daftar_nilai WHERE idsiswa='$id_siswa' AND idmapel='1' AND idnilai='2'");
        $uts=mysqli_fetch_array($sqlnilai);
        if($count=mysqli_num_rows($sqlnilai)){
        ?>
        <div class="text-center">
        <?php echo $uts['nilai'];?>
        </div>
        <?php }else{
            ?>
            <div class="text-center">0</div>
        <?php }?>
        </td>
        <td>
        <?php 
        $sqlnilai=mysqli_query($conn,"SELECT *FROM daftar_nilai WHERE idsiswa='$id_siswa' AND idmapel='1' AND idnilai='3'");
        $uas=mysqli_fetch_array($sqlnilai);
        if($count=mysqli_num_rows($sqlnilai)){
        ?>
        <div class="text-center">
        <?php echo $uas['nilai'];?>
        </div>
        <?php }else{
            ?>
            <div class="text-center">0</div>
        <?php }?>
        </td>
        <td>
            <?php
            $tugas=$tugas['nilai'];
            $uh=$uh['nilai'];
            $uts=$uts['nilai'];
            $uas=$uas['nilai'];
            $akhir=($tugas+$uh+$uts+$uas) /4;
            if($uh =="" || $uts =="" || $uas ==""){
                    echo "<h6 class='text-center'>-</h6>";                
            }else{

                echo "<h6 class='text-center'>".$akhir."</h6>";                
            }
            ?>
        </td>
        
		<!-- <td><a href="nilai_siswa.php?nis=<?php echo $datas['nis']?>" class="btn btn-info" href="#" role="button"> <i class="fa fa-eye" aria-hidden="true" title="Lihat Nilai <?php echo $datas['nama']?>"></i>   </a><!---&nbsp;|&nbsp;--->
		<!-- <a class="btn btn-danger" href="delete_kelas.php?id=<?php echo $datak['id']?>" title="Delete"><i class="fa fa-trash"></i></a> 
		<a name="" id="" class="btn btn-primary" href="" title="edit"><i class="fa fa-edit    "></i></a> -->
		
	</tr>
	<!-- end -->
	<?php		
		}else{
			?>
				<tr>
				<td colspan="9" class="text-center"><h3>Data Kosong</h3></td>
				</tr>
			<?php
		}
	?>
	</tbody>
</table>

<!-- bing -->
<table class="table table-hover table-stripped">
	<thead class="thead-dark">
        <tr>
        <th colspan="5" class="text-center">Bahasa Inggris</th>
        </tr>
		<tr class="text-center">
        <th>Tugas</th>
        <th>UH</th>
        <th>UTS</th>
		<th>UAS</th>
        <th>Nilai Akhir</th>
	</tr>
	</thead>
	<tbody>
	<?php
	$data_siswa=mysqli_query($conn,"SELECT * FROM daftar_nilai WHERE idsiswa='$id_siswa' AND idmapel='2' ");
	$no=1;
	$count=mysqli_num_rows($data_siswa);
	if( $count >0){
		$datas=mysqli_fetch_array($data_siswa);
			
	?>
	<tr>
        <td>
        <?php 
        $sqlnilai=mysqli_query($conn,"SELECT *FROM daftar_nilai WHERE idsiswa='$id_siswa' AND idmapel='2' AND idnilai='4'");
        $tugas=mysqli_fetch_array($sqlnilai);
        if($count=mysqli_num_rows($sqlnilai)){
        ?>
        <div class="text-center">
        <?php echo $tugas['nilai'];?>
        </div>
        <?php }else{
            ?>
            <div class="text-center">0</div>
        <?php }?>
        </td>
        <td>
        <?php 
        $sqlnilai=mysqli_query($conn,"SELECT *FROM daftar_nilai WHERE idsiswa='$id_siswa' AND idmapel='2' AND idnilai='1'");
        $uh=mysqli_fetch_array($sqlnilai);
        if($count=mysqli_num_rows($sqlnilai)){
        ?>
        <div class="text-center">
        <?php echo $uh['nilai'];?>
        </div>
        <?php }else{
            ?>
            <div class="text-center">0</div>
        <?php }?>
        </td>
        <td>
        <?php 
        $sqlnilai=mysqli_query($conn,"SELECT *FROM daftar_nilai WHERE idsiswa='$id_siswa' AND idmapel='2' AND idnilai='2'");
        $uts=mysqli_fetch_array($sqlnilai);
        if($count=mysqli_num_rows($sqlnilai)){
        ?>
        <div class="text-center">
        <?php echo $uts['nilai'];?>
        </div>
        <?php }else{
            ?>
            <div class="text-center">0</div>
        <?php }?>
        </td>
        <td>
        <?php 
        $sqlnilai=mysqli_query($conn,"SELECT *FROM daftar_nilai WHERE idsiswa='$id_siswa' AND idmapel='3' AND idnilai='3'");
        $uas=mysqli_fetch_array($sqlnilai);
        if($count=mysqli_num_rows($sqlnilai)){
        ?>
        <div class="text-center">
        <?php echo $uas['nilai'];?>
        </div>
        <?php }else{
            ?>
            <div class="text-center">0</div>
        <?php }?>
        </td>
        <td>
            <?php
            $tugas=$tugas['nilai'];
            $uh=$uh['nilai'];
            $uts=$uts['nilai'];
            $uas=$uas['nilai'];
            $akhir=($tugas+$uh+$uts+$uas) /4;
            if($uh =="" || $uts =="" || $uas ==""){
                    echo "<h6 class='text-center'>-</h6>";                
            }else{

                echo "<h6 class='text-center'>".$akhir."</h6>";                
            }
            ?>
        </td>
		<!-- <td><a href="nilai_siswa.php?nis=<?php echo $datas['nis']?>" class="btn btn-info" href="#" role="button"> <i class="fa fa-eye" aria-hidden="true" title="Lihat Nilai <?php echo $datas['nama']?>"></i>   </a><!---&nbsp;|&nbsp;--->
		<!-- <a class="btn btn-danger" href="delete_kelas.php?id=<?php echo $datak['id']?>" title="Delete"><i class="fa fa-trash"></i></a> 
		<a name="" id="" class="btn btn-primary" href="" title="edit"><i class="fa fa-edit    "></i></a> -->
		</td>
	</tr>
	<!-- end -->
	<?php		
		}else{
			?>
				<tr>
				<td colspan="9" class="text-center"><h3>Data Kosong</h3></td>
				</tr>
			<?php
		}
	?>
	</tbody>
</table>
<!-- bindo -->
<table class="table table-hover table-stripped">
	<thead class="thead-dark">
        <tr>
        <th colspan="5" class="text-center">Bahasa Indonesia</th>
        </tr>
		<tr class="text-center">
        <th>Tugas</th>
        <th>UH</th>
        <th>UTS</th>
		<th>UAS</th>
        <th>Nilai Akhir</th>
	</tr>
	</thead>
	<tbody>
	<?php
	$data_siswa=mysqli_query($conn,"SELECT * FROM daftar_nilai WHERE idsiswa='$id_siswa' AND idmapel='3' ");
	$no=1;
	$count=mysqli_num_rows($data_siswa);
	if( $count >0){
		$datas=mysqli_fetch_array($data_siswa);
			
	?>
	<tr>
        <td>
        <?php 
        $sqlnilai=mysqli_query($conn,"SELECT *FROM daftar_nilai WHERE idsiswa='$id_siswa' AND idmapel='3' AND idnilai='4'");
        $tugas=mysqli_fetch_array($sqlnilai);
        if($count=mysqli_num_rows($sqlnilai)){
        ?>
        <div class="text-center">
        <?php echo $tugas['nilai'];?>
        </div>
        <?php }else{
            ?>
            <div class="text-center">0</div>
        <?php }?>
        </td>
        <td>
        <?php 
        $sqlnilai=mysqli_query($conn,"SELECT *FROM daftar_nilai WHERE idsiswa='$id_siswa' AND idmapel='3' AND idnilai='1'");
        $tugas=mysqli_fetch_array($sqlnilai);
        if($count=mysqli_num_rows($sqlnilai)){
        ?>
        <div class="text-center">
        <?php echo $tugas['nilai'];?>
        </div>
        <?php }else{
            ?>
            <div class="text-center">0</div>
        <?php }?>
        </td>
        <td>
        <?php 
        $sqlnilai=mysqli_query($conn,"SELECT *FROM daftar_nilai WHERE idsiswa='$id_siswa' AND idmapel='3' AND idnilai='2'");
        $tugas=mysqli_fetch_array($sqlnilai);
        if($count=mysqli_num_rows($sqlnilai)){
        ?>
        <div class="text-center">
        <?php echo $tugas['nilai'];?>
        </div>
        <?php }else{
            ?>
            <div class="text-center">0</div>
        <?php }?>
        </td>
        <td>
        <?php 
        $sqlnilai=mysqli_query($conn,"SELECT *FROM daftar_nilai WHERE idsiswa='$id_siswa' AND idmapel='3' AND idnilai='3'");
        $tugas=mysqli_fetch_array($sqlnilai);
        if($count=mysqli_num_rows($sqlnilai)){
        ?>
        <div class="text-center">
        <?php echo $tugas['nilai'];?>
        </div>
        <?php }else{
            ?>
            <div class="text-center">0</div>
        <?php }?>
        </td>
		<!-- <td><a href="nilai_siswa.php?nis=<?php echo $datas['nis']?>" class="btn btn-info" href="#" role="button"> <i class="fa fa-eye" aria-hidden="true" title="Lihat Nilai <?php echo $datas['nama']?>"></i>   </a><!---&nbsp;|&nbsp;--->
		<!-- <a class="btn btn-danger" href="delete_kelas.php?id=<?php echo $datak['id']?>" title="Delete"><i class="fa fa-trash"></i></a> 
		<a name="" id="" class="btn btn-primary" href="" title="edit"><i class="fa fa-edit    "></i></a> -->
		</td>
	</tr>
	<!-- end -->
	<?php		
		}else{
			?>
				<tr>
				<td colspan="9" class="text-center"><h3>Data Kosong</h3></td>
				</tr>
			<?php
		}
	?>
	</tbody>
</table>
</div>
<!-- /edtable -->
</body>
</html>
<?php }?>