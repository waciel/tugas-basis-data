<?php
	include '../koneksi.php';
	session_start();
	if (!isset($_SESSION['nip'])) {
		header("location:loginguru.php");
	}else{
?>
<!DOCTYPE html>
<html>
<head>
	<title>Data Siswa - SMKKN 01 CIBINONG</title>
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/themify-icons.css">
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/font-awesome.min.css">
	<script src="../bootstrap/js/popper.min.js"></script>
	<script src="../bootstrap/js/bootstrap.min.js"></script>
	<script src="../bootstrap/js/bootstrap.js"></script>
	<script src="../bootstrap/js/jquery-3.3.1.slim.min.js"></script>
	<!-- cdn anjay -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<!-- //cdn -->
</head>
<body>
<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-dark bg-primary shadow fixed-top" style="background: linear-gradient(141deg, #9fb8ad 0%, #1fc8db 51%, #2cb5e8 10%);">
  <a class="navbar-brand" href="#">SMK Indonesia</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse ml-5" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="home.php" style="font-size: 19px;">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item ">
        <a class="nav-link" href="data_siswa.php" style="font-size: 19px;">Data Siswa</a>
      </li>
      <li class="nav-item active" >
        <a class="nav-link" href="data_guru.php" style="font-size: 19px;">Data Guru</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="data_nilai.php" style="font-size: 19px;">Data Nilai</a>
      </li>
    </ul>
    <ul class="navbar-nav flex-row ml-md-auto d-none d-md-flex">
	<div class="dropdown">
	  <button class="btn btn-transparent dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border:none; color: #fff; font-size: 18px;">
	    Hello, <?php echo $_SESSION['nama']?>!!
	  </button>
	  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
	    <a class="dropdown-item" href="#">Logout</a>
	    <a class="dropdown-item" href="#">Another action</a>
	    <a class="dropdown-item" href="#">Something else here</a>
	  </div>
	</div>
    </ul>
  </div>
</nav>
<!-- //Nabar END -->
<!-- tabel -->
<div class="container" style="margin-top: 200px;">
	<div class="row md-12">
	<div class="col-md-6">
		<a class="btn btn-success" href="tambah_data_guru" role="button" title="Tambah Data Guru"> <i class="fa fa-user-plus" aria-hidden="true"></i></a>
	</div>
		<div class="col-md-6">
			<form action="" method="GET">
				<div class="input-group mb-2">
		        <div class="input-group-prepend">
		          <div class="input-group-text search"><i class="fa fa-search"></i></div>
		        </div>
				<input type="search" name="cari" placeholder="Cari Nip, Nama dan Alamat" class="form-control">
			</form>

		</div>
	</div>
<table class="table table-borderless table-hover table-stripped">
	<thead class="thead-light">
		<tr>
		<th>No</th>
		<th>NIP</th>
		<th>Nama Lengkap</th>
		<th>Mengajar Kelas</th>
		<th>Mengajar Mapel</th>
		<Th>Alamat</Th>		
	</tr>
	</thead>
	<?php
		if (isset($_GET['cari'])) {
			$cari=$_GET['cari'];
			$search=mysqli_query($conn,"SELECT * FROM guru WHERE nip LIKE'%".$cari."%' OR nama LIKE '%".$cari."%' OR alamat LIKE'%".$cari."%'");
		}else{
			$search=mysqli_query($conn,"SELECT * FROM guru");
		}	
		$no=1;
		if ($count=mysqli_num_rows($search)) {			
		while($data=mysqli_fetch_array($search)) {			
	?>
	<?php
	if($_SESSION['nip'] == $data['nip']){
		?>
		<tr style="background-color: blue; color: white; font-weight: bold;">
		<td><?php echo $no++?></td>
		<TD><?php echo $data['nip']?></TD>
		<td><?php echo $data['nama']?></td>
		<td>
			<?php
			$nip=$_SESSION['nip'];
				$cari=mysqli_query($conn,"SELECT * FROM guru WHERE nip='$nip' AND id_role='1'");
				$datacari=mysqli_fetch_array($cari);
				$idguru=$datacari['id'];

				$mengajar=mysqli_query($conn,"SELECT * FROM mengajar WHERE idguru='$idguru'");
				if($count=mysqli_num_rows($mengajar)){
					while($datam=mysqli_fetch_array($mengajar)){
			?>
				<tr>
						<Td><?php echo $datam['idmapel']?></Td>
				</tr>
			<?php }}else{?>
					<Td>-</Td>				
			<?php }?>
		</td>
		<td><?php echo $data['alamat']?></td>
		</tr>
		<?php
	}else{
	?>
	<tr>
		<td><?php echo $no++?></td>
		<TD><?php echo $data['nip']?></TD>
		<td><?php echo $data['nama']?></td>
		<td><?php echo $data['alamat']?></td>
	</tr>
<?php }}}else{?>
	<tr>
		<td class="text-center" colspan="8"><h3>Data Kosong</h3></td>
	</tr>
<?php }?>
</table>
</div>
<!-- /edtable -->
</body>
</html>
<?php }?>