<?php
	include '../koneksi.php';
	session_start();
	if (!isset($_SESSION['nip'])) {
		header("location:loginguru.php");
	}else{
?>
<!DOCTYPE html>
<html>
<head>
	<title>Data Siswa - SMKKN 01 CIBINONG</title>
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/themify-icons.css">
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/font-awesome.min.css">
	<script src="../bootstrap/js/popper.min.js"></script>
	<script src="../bootstrap/js/bootstrap.min.js"></script>
	<script src="../bootstrap/js/bootstrap.js"></script>
	<script src="../bootstrap/js/jquery-3.3.1.slim.min.js"></script>
	<!-- cdn anjay -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<!-- //cdn -->
</head>
<body>
<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-dark bg-primary shadow fixed-top" style="background: linear-gradient(141deg, #9fb8ad 0%, #1fc8db 51%, #2cb5e8 10%);">
  <a class="navbar-brand" href="#">SMK Indonesia</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse ml-5" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="home.php" style="font-size: 19px;">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="data_siswa.php" style="font-size: 19px;">Data Siswa</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="data_guru.php" style="font-size: 19px;">Data Guru</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="data_nilai.php" style="font-size: 19px;">Data Nilai</a>
      </li>
    </ul>
    <ul class="navbar-nav flex-row ml-md-auto d-none d-md-flex">
	<div class="dropdown">
	  <button class="btn btn-transparent dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border:none; color: #fff; font-size: 18px;">
	    Hello, <?php echo $_SESSION['nama']?>!!
	  </button>
	  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
	    <a class="dropdown-item" href="#">Logout</a>
	    <a class="dropdown-item" href="#">Another action</a>
	    <a class="dropdown-item" href="#">Something else here</a>
	  </div>
	</div>
    </ul>
  </div>
</nav>
<!-- //Nabar END -->
<!-- tabel -->
<div class="container" style="margin-top: 200px;">
	<div class="row md-12">
		<div class="col-md-6">
			<a href="tambah_data_siswa.php" class="btn btn-success">Tambah Data Siswa</a><br><br>
		</div>
		<div class="col-md-6">
			<div class="row-md-12 mb-2">
				<div class="col-md-6">
				<!-- Button trigger modal -->
			<button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#exampleModal">
			  Id kelas
			</button>

			<!-- Modal -->
			<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header" style="border:none;">
			        <h5 class="modal-title" id="exampleModalLabel">Id Kelas</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">
			        <?php
			        $query=mysqli_query($conn,"SELECT * FROM kelas");
			        $count=mysqli_num_rows($query);
			        if ($count >0) {
			        	while ($data=mysqli_fetch_array($query)) {
			        		?>
						<table class="table table-striped table-hover">
							<tr>
								<td><?php echo $data['tingkat']." ".$data['idprodi']." ".$data['nomor']?></td>
								<td>=</td>
								<td><?php echo $data['id']?></td>
							</tr>
						</table>
			        		<?php
			        	}
			        }
			        ?>
			      </div>
			      <div class="modal-footer" style="border:none;">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>			        
			      </div>
			    </div>
			  </div>
			</div>
				</div>
			</div>
			<form action="" method="GET">
				<div class="input-group mb-2">
		        <div class="input-group-prepend">
		          <div class="input-group-text search"><i class="fa fa-search"></i></div>
		        </div>
				<input type="search" name="cari" placeholder="Cari Nis, Nama, Jurusan, atau Id kelas" class="form-control">
			</form>

		</div>
	</div>	
<table class="table table-bordered table-hover table-stripped">
	<thead class="thead-light">
		<tr>
		<th>No</th>
		<th>NIS</th>
		<th>Nama Lengkap</th>
		<Th>Jenis Kelamin</Th>
		<Th>Jurusan</Th>
		<th>Tingkatan</th>
		<th>Kelas</th>
		<th>Option</th>
	</tr>
	</thead>
	<!-- nis LIKE'%".$cari."%' OR  -->
	<?php
		if (isset($_GET['cari'])) {
			$cari=$_GET['cari'];
			$search=mysqli_query($conn,"SELECT * FROM siswa JOIN kelas ON siswa.idkelas=kelas.id WHERE nama LIKE '%".$cari."%' OR nis LIKE '".$cari."' OR idprodi LIKE'%".$cari."%' OR kelas.id LIKE '%".$cari."%'");
		}else{
			$search=mysqli_query($conn,"SELECT * FROM siswa JOIN kelas ON siswa.idkelas=kelas.id");
		}	
		$no=1;
		if ($count=mysqli_num_rows($search)) {			
		while($data=mysqli_fetch_array($search)) {			
	?>
	<tr>
		<td><?php echo $no++?></td>
		<TD><?php echo $data['nis']?></TD>
		<td><?php echo $data['nama']?></td>
		<td><?php echo $data['jeniskelamin']?></td>
		<td><?php echo $data['idprodi']?></td>
		<TD><?php echo $data['tingkat']?></TD>
		<td><?php echo $data['tingkat']." ".$data['idprodi']." ". $data['nomor']?></td>
		<td><a href="edit_data_siswa.php?nis=<?php echo $data['nis']?>" class="btn btn-primary" title="Edit"><i class="fa fa-edit"></i></a> <a href="delete_siswa.php?nis=<?php echo $data['nis']?>" class="btn btn-danger" onclick="return confirm('Are You Sure?');" title="Delete"><i class="fa fa-trash"></i></a></td>
	</tr>
<?php }}else{?>
	<tr>
		<td class="text-center" colspan="8"><h3>Data Kosong</h3></td>
	</tr>
<?php }?>
</table>
</div>
<!-- /edtable -->
</body>
</html>
<?php }?>