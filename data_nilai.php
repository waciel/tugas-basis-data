<?php
  include 'koneksi.php';
  session_start();
  if (!isset($_SESSION['nis'])) {
    header("location:login.php");
  }else{
?>
<!DOCTYPE html>
<html>
<head>
	<title>Data Siswa - SMKKN 01 CIBINONG</title>
	<link rel="stylesheet" type="text/css" href="./bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="./bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="./bootstrap/css/themify-icons.css">
	<link rel="stylesheet" type="text/css" href="./bootstrap/css/font-awesome.min.css">
	<script src="./bootstrap/js/popper.min.js"></script>
	<script src="./bootstrap/js/bootstrap.min.js"></script>
	<script src="./bootstrap/js/bootstrap.js"></script>
	<script src="./bootstrap/js/jquery-3.3.1.slim.min.js"></script>
	<!-- cdn anjay -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<!-- //cdn -->
</head>
<body>
<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-dark bg-primary shadow fixed-top" style="background: linear-gradient(141deg, #9fb8ad 0%, #1fc8db 51%, #2cb5e8 10%);">
  <a class="navbar-brand" href="#">SMK Indonesia</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse ml-5" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="./home.php" style="font-size: 19px;">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item ">
        <a class="nav-link" href="data_siswa.php" style="font-size: 19px;">Data Siswa</a>
      </li>
      <li class="nav-item " >
        <a class="nav-link" href="data_guru.php" style="font-size: 19px;">Data Guru</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="data_nilai.php" style="font-size: 19px;">Data Nilai</a>
      </li>
    </ul>
    <ul class="navbar-nav flex-row ml-md-auto d-none d-md-flex">
	<div class="dropdown">
	  <button class="btn btn-transparent dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border:none; color: #fff; font-size: 18px;">
	    Hello, Wahyu Aditya Rizki!!
	  </button>
	  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
	    <a class="dropdown-item" href="#">Logout</a>
	    <a class="dropdown-item" href="#">Another action</a>
	    <a class="dropdown-item" href="#">Something else here</a>
	  </div>
	</div>
    </ul>
  </div>
</nav>
<!-- //Nabar END -->
<!-- tabel -->
<div class="container" style="margin-top: 200px;">	
<table class="table table-bordered table-hover table-stripped">
	<thead class="thead-dark">
		<tr>
		<th>No</th>
		<th>Nama Siswa</th>
		<th>Mapel</th>
		<Th>Nama Guru</Th>
		<th>Nilai</th>
	</tr>
	</th>
	
	<tr>
		<td>1</td>
		<TD>Siti Azzahra</TD>
		<td>Fisika</td>		
		<td>Robiyyah</td>
		<td>80</td>
	</tr>
</table>
</div>
<!-- /edtable -->
</body>
</html>
<?php }?>