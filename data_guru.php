<?php
  include 'koneksi.php';
  session_start();
  if (!isset($_SESSION['nis'])) {
    header("location:login.php");
  }else{
?>
<!DOCTYPE html>
<html>
<head>
	<title>Data Siswa - SMKKN 01 CIBINONG</title>
	<link rel="stylesheet" type="text/css" href="./bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="./bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="./bootstrap/css/themify-icons.css">
	<link rel="stylesheet" type="text/css" href="./bootstrap/css/font-awesome.min.css">
	<script src="./bootstrap/js/popper.min.js"></script>
	<script src="./bootstrap/js/bootstrap.min.js"></script>
	<script src="./bootstrap/js/bootstrap.js"></script>
	<script src="./bootstrap/js/jquery-3.3.1.slim.min.js"></script>
	<!-- cdn anjay -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<!-- //cdn -->
</head>
<body>
<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-dark bg-primary shadow fixed-top" style="background: linear-gradient(141deg, #9fb8ad 0%, #1fc8db 51%, #2cb5e8 10%);">
  <a class="navbar-brand" href="#">SMK Indonesia</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse ml-5" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="./home.php" style="font-size: 19px;">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item ">
        <a class="nav-link" href="data_siswa.php" style="font-size: 19px;">Data Siswa</a>
      </li>
      <li class="nav-item active" >
        <a class="nav-link" href="data_guru.php" style="font-size: 19px;">Data Guru</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="data_nilai.php" style="font-size: 19px;">Data Nilai</a>
      </li>
    </ul>
    <ul class="navbar-nav flex-row ml-md-auto d-none d-md-flex">
	<div class="dropdown">
	  <button class="btn btn-transparent dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border:none; color: #fff; font-size: 18px;">
	    Hello, <?php echo $_SESSION['nama']?>!!
	  </button>
	  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
	    <a class="dropdown-item" href="logout.php" onclick="return confirm('Apakah Anda Yakin Ingin Keluar?')">Logout</a>
	    <!-- <a class="dropdown-item" href="#">Another action</a>
	    <a class="dropdown-item" href="#">Something else here</a> -->
	  </div>
	</div>
    </ul>
  </div>
</nav>
<!-- //Nabar END -->
<!-- tabel -->
<div class="container" style="margin-top: 200px;">
	<div class="row-md-12">
		<div class="col-md-6"></div>
		<div class="col-md-6">
			<form action="" method="GET" class="form-group">
				<div class="input-group mb-2">
		        <div class="input-group-prepend">
		          <div class="input-group-text search"><i class="fa fa-search"></i></div>
		        </div>
				<input type="search" name="cari" class="form-control" placeholder="Cari Nip, atau Nama Lengkap">
			</form>
		</div>
		</div>
	</div>	
<table class="table table-hover table-striped">
	<thead class="thead-dark">
		<tr>
		<th>No</th>
		<th>NIP</th>
		<th>Nama Lengkap</th>
		<Th>Alamat</Th>
	</tr>
	</thead>
	<?php
	if (isset($_GET['cari'])) {
		$cari=$_GET['cari'];
		$cari=mysqli_query($conn,"SELECT * FROM guru WHERE nip LIKE '%".$cari."%' OR  nama LIKE '%".$cari."%' ORDER BY nip ASC");
	}else{
		$cari=mysqli_query($conn,"SELECT * FROM guru");
	}
	$no=1;	
	$count=mysqli_num_rows($cari);
	if ($count >0) {
		while ($data=mysqli_fetch_array($cari)) {
	?>
	<tr scope="row">
		<td><?php echo $no++?></td>
		<TD><?php echo $data['nip']?></TD>
		<td><?php echo $data['nama']?></td>
		<TD><?php echo $data['alamat']?></TD>
	</tr>
	<?php }}?>
</table>
</div>
<!-- /edtable -->
</body>
</html>
<?php }?>