-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 10 Nov 2019 pada 03.46
-- Versi Server: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `erd`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `daftar_nilai`
--

CREATE TABLE `daftar_nilai` (
  `id_daftar_nilai` int(11) NOT NULL,
  `idguru` int(11) NOT NULL,
  `idsiswa` int(11) NOT NULL,
  `idmapel` int(11) NOT NULL,
  `uh` int(3) NOT NULL,
  `uts` int(3) NOT NULL,
  `uas` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `daftar_nilai`
--

INSERT INTO `daftar_nilai` (`id_daftar_nilai`, `idguru`, `idsiswa`, `idmapel`, `uh`, `uts`, `uas`) VALUES
(74, 3, 15, 3, 80, 100, 72),
(75, 3, 14, 3, 80, 80, 80),
(92, 1, 15, 1, 90, 90, 90),
(93, 1, 15, 2, 90, 90, 90),
(94, 1, 14, 1, 90, 90, 90),
(95, 1, 14, 2, 90, 90, 90);

-- --------------------------------------------------------

--
-- Struktur dari tabel `guru`
--

CREATE TABLE `guru` (
  `id` int(11) NOT NULL,
  `nip` int(20) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `alamat` varchar(30) NOT NULL,
  `password` varchar(60) DEFAULT NULL,
  `id_role` int(3) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `guru`
--

INSERT INTO `guru` (`id`, `nip`, `nama`, `alamat`, `password`, `id_role`) VALUES
(1, 1, 'Bambang', 'Jakarta', 'haha', 1),
(2, 111, 'Mansur', 'Bandung', 'w', 1),
(3, 222, 'Suleman', 'Surabaya', 's', 1),
(4, 81818, 'Atang', 'Bandung', 'weng', 1),
(5, 9090, 'Nurul', 'Bandung', 'pass', 1),
(6, 11107, 'Jajang Nurjaman', 'Cimaung', 'jajangoke', 2),
(7, 201, 'Riko Simajuntak', 'Jakarta', 'rikoenak', 1),
(8, 10102020, 'Siti Zahra', 'Citayam', 'sitizhra', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelas`
--

CREATE TABLE `kelas` (
  `id` int(11) NOT NULL,
  `idprodi` varchar(5) NOT NULL,
  `tingkat` int(2) NOT NULL,
  `nomor` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kelas`
--

INSERT INTO `kelas` (`id`, `idprodi`, `tingkat`, `nomor`) VALUES
(1, 'TKR', 12, 1),
(4, 'TKR', 12, 2),
(5, 'TKR', 12, 3),
(6, 'MM', 12, 1),
(7, 'MM', 12, 2),
(8, 'MM', 12, 3),
(9, 'MM', 11, 1),
(10, 'MM', 11, 2),
(11, 'MM', 11, 2),
(12, 'TKR', 11, 1),
(13, 'TKR', 11, 2),
(14, 'TKR', 11, 3),
(15, 'RPL', 12, 1),
(16, 'RPL', 12, 2),
(17, 'RPL', 11, 1),
(18, 'RPL', 11, 2),
(19, 'RPL', 10, 1),
(20, 'RPL', 10, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `mapel`
--

CREATE TABLE `mapel` (
  `id` int(11) NOT NULL,
  `nama_mapel` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mapel`
--

INSERT INTO `mapel` (`id`, `nama_mapel`) VALUES
(1, 'Matematika'),
(2, 'Bahasa Inggris'),
(3, 'Bahasa Indonesia'),
(4, 'PAI');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mengajar`
--

CREATE TABLE `mengajar` (
  `id` int(11) NOT NULL,
  `idguru` int(11) NOT NULL,
  `idmapel` int(11) NOT NULL,
  `idkelas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mengajar`
--

INSERT INTO `mengajar` (`id`, `idguru`, `idmapel`, `idkelas`) VALUES
(17, 1, 1, 6),
(18, 1, 2, 6),
(19, 3, 3, 6),
(20, 1, 4, 6),
(21, 1, 1, 8),
(22, 1, 1, 9);

-- --------------------------------------------------------

--
-- Struktur dari tabel `nilai`
--

CREATE TABLE `nilai` (
  `id` int(11) NOT NULL,
  `nama` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nilai`
--

INSERT INTO `nilai` (`id`, `nama`) VALUES
(1, 'UH'),
(2, 'UTS'),
(3, 'UAS'),
(4, 'Tugas');

-- --------------------------------------------------------

--
-- Struktur dari tabel `prodi`
--

CREATE TABLE `prodi` (
  `id_prodi` varchar(4) NOT NULL,
  `nama` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `prodi`
--

INSERT INTO `prodi` (`id_prodi`, `nama`) VALUES
('MM', 'Multimedia'),
('RPL', 'Rekayasa Perangkat Lunak'),
('TKR', 'Teknik Kendaraan Ringan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `role`
--

CREATE TABLE `role` (
  `id_role` int(3) NOT NULL,
  `nama_role` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `role`
--

INSERT INTO `role` (`id_role`, `nama_role`) VALUES
(1, 'guru'),
(2, 'admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `siswa`
--

CREATE TABLE `siswa` (
  `id` int(11) NOT NULL,
  `idkelas` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `nis` int(10) NOT NULL,
  `jeniskelamin` varchar(10) NOT NULL,
  `password` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `siswa`
--

INSERT INTO `siswa` (`id`, `idkelas`, `nama`, `nis`, `jeniskelamin`, `password`) VALUES
(3, 1, 'James Waliyudin', 12345, 'Laki-Laki', 'ja'),
(5, 9, 'William Van Kuyt', 1017007702, 'Laki-Laki', 'baby'),
(11, 9, 'Sunfo Sanchez', 101010, 'Perempuan', 'pas'),
(13, 8, 'Udin', 123, 'Laki-Laki', 'udin'),
(14, 6, 'lkfwwj', 12456, 'fkw', 'test'),
(15, 6, 'Khazanah Sophie', 2147483647, 'Perempuan', NULL),
(16, 9, 'KIya', 89182919, 'Laki-Laki', NULL),
(17, 9, 'ghfhg', 788, 'Laki-Laki', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `daftar_nilai`
--
ALTER TABLE `daftar_nilai`
  ADD PRIMARY KEY (`id_daftar_nilai`),
  ADD KEY `idguru` (`idguru`),
  ADD KEY `idsiswa` (`idsiswa`),
  ADD KEY `idmapel` (`idmapel`);

--
-- Indexes for table `guru`
--
ALTER TABLE `guru`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_role` (`id_role`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idprodi` (`idprodi`);

--
-- Indexes for table `mapel`
--
ALTER TABLE `mapel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mengajar`
--
ALTER TABLE `mengajar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idguru` (`idguru`),
  ADD KEY `idmapel` (`idmapel`),
  ADD KEY `idkelas` (`idkelas`);

--
-- Indexes for table `nilai`
--
ALTER TABLE `nilai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prodi`
--
ALTER TABLE `prodi`
  ADD PRIMARY KEY (`id_prodi`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id_role`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idkelas` (`idkelas`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `daftar_nilai`
--
ALTER TABLE `daftar_nilai`
  MODIFY `id_daftar_nilai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;

--
-- AUTO_INCREMENT for table `guru`
--
ALTER TABLE `guru`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `kelas`
--
ALTER TABLE `kelas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `mapel`
--
ALTER TABLE `mapel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `mengajar`
--
ALTER TABLE `mengajar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `nilai`
--
ALTER TABLE `nilai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id_role` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `siswa`
--
ALTER TABLE `siswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `daftar_nilai`
--
ALTER TABLE `daftar_nilai`
  ADD CONSTRAINT `daftar_nilai_ibfk_2` FOREIGN KEY (`idsiswa`) REFERENCES `siswa` (`id`),
  ADD CONSTRAINT `daftar_nilai_ibfk_3` FOREIGN KEY (`idguru`) REFERENCES `guru` (`id`),
  ADD CONSTRAINT `daftar_nilai_ibfk_4` FOREIGN KEY (`idmapel`) REFERENCES `mapel` (`id`);

--
-- Ketidakleluasaan untuk tabel `guru`
--
ALTER TABLE `guru`
  ADD CONSTRAINT `guru_ibfk_1` FOREIGN KEY (`id_role`) REFERENCES `role` (`id_role`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `kelas`
--
ALTER TABLE `kelas`
  ADD CONSTRAINT `kelas_ibfk_1` FOREIGN KEY (`idprodi`) REFERENCES `prodi` (`id_prodi`);

--
-- Ketidakleluasaan untuk tabel `mengajar`
--
ALTER TABLE `mengajar`
  ADD CONSTRAINT `mengajar_ibfk_1` FOREIGN KEY (`idmapel`) REFERENCES `mapel` (`id`),
  ADD CONSTRAINT `mengajar_ibfk_3` FOREIGN KEY (`idguru`) REFERENCES `guru` (`id`),
  ADD CONSTRAINT `mengajar_ibfk_4` FOREIGN KEY (`idkelas`) REFERENCES `kelas` (`id`);

--
-- Ketidakleluasaan untuk tabel `siswa`
--
ALTER TABLE `siswa`
  ADD CONSTRAINT `siswa_ibfk_1` FOREIGN KEY (`idkelas`) REFERENCES `kelas` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
